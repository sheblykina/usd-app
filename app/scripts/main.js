/*global require*/
'use strict';

require.config({
    shim: {
        bootstrapmin: {deps: ['jquery']},
        'jquery.tablesorter': {deps: ['jquery']},
        'bootstrap-table': {deps: ['jquery']},

        // this is for sync bootstrap loading
        // http://getfishtank.ca/blog/load-bootstrap-3-javascript-components-using-requirejs
        'bootstrap/affix': {deps: ['jquery'], exports: '$.fn.affix'},
        'bootstrap/alert': {deps: ['jquery'], exports: '$.fn.alert'},
        'bootstrap/button': {deps: ['jquery'], exports: '$.fn.button'},
        'bootstrap/carousel': {deps: ['jquery'], exports: '$.fn.carousel'},
        'bootstrap/collapse': {deps: ['jquery'], exports: '$.fn.collapse'},
        'bootstrap/dropdown': {deps: ['jquery'], exports: '$.fn.dropdown'},
        'bootstrap/modal': {deps: ['jquery'], exports: '$.fn.modal'},
        'bootstrap/popover': {deps: ['jquery'], exports: '$.fn.popover'},
        'bootstrap/scrollspy': {deps: ['jquery'], exports: '$.fn.scrollspy'},
        'bootstrap/tab': {deps: ['jquery'], exports: '$.fn.tab'},
        'bootstrap/tooltip': {deps: ['jquery'], exports: '$.fn.tooltip'},
        'bootstrap/transition': {deps: ['jquery'], exports: '$.fn.transition'},
        'bfh/bootstrap-formhelpers-phone': {deps: ['jquery']},
        'jquery-plugins-numeric': {deps: ['jquery']}
    },
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/lodash/dist/lodash',
        bootstrap: '../bower_components/bootstrap/js',
        'jquery.tablesorter': '../bower_components/tablesorter/jquery.tablesorter.min',
        bootstrapmin: '../bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-table': '../bower_components/bootstrap-table/dist/bootstrap-table',
        'bfh': '../bower_components/bootstrap-form-helpers/js',
        'js-cookie': '../bower_components/js-cookie/src/js.cookie',
        'jquery-plugins-numeric':'../bower_components/jQuery-Plugins/numeric/jquery.numeric.min'
    }
});

require([
    'app'
], function (app) {
    app.run();
});

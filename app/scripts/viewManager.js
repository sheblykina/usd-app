define(function (require) {
    'use strict';

    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');

    var ViewManager = function () {
        return {
            show: showView
        };
    };

    _.extend(ViewManager, Backbone.Events);

    function showView(view) {

        if (this.currentView) {
            disposeView(this.currentView);
        }

        this.currentView = view;

        $("#app").html(this.currentView.el);

        this.currentView.render();
    }

    function disposeView(view) {
        _.each(view.subviews, function(subview) {
            disposeView(subview);
        });

        view.remove();
    }

    return ViewManager;

});

/**
 * Created by andreybobrov on 2/6/16.
 */
define(function(require){
    'use strict';
    var $ = require("jquery");
    var globals = require("../globalConfig");
    var authManager = {};

    authManager.checkUserAuth = function() {
        var checkUri = globals.authCheckUri;
        var def = $.Deferred();

        $.ajax({
            type:"get",

            url: checkUri,

            success: function () {
                def.resolve();
            },

            error: function () {
                def.reject();
            }
        });
        return def.promise();
    };

    return authManager;
});

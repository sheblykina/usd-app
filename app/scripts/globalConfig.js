/**
 * Created by andreybobrov on 8/6/15.
 */
define(function(require) {
    'use strict';

    var globals = {};

    globals.baseUrl = 'http://127.0.0.1:3000/';

    globals.marketBaseUri = globals.baseUrl + 'market/';
    globals.usdApiUri = globals.marketBaseUri + 'kiev/usd';
    globals.authUri = globals.baseUrl + 'auth/';
    globals.userOffersUri = globals.baseUrl + 'user';

    globals.authCheckUri = globals.baseUrl + 'authCheck';

    var signs = globals.signs = Object.create(null);

    signs['rub'] = "&#x20bd;";
    signs['eur'] = "&#8364;";
    signs['usd'] = "&#36;";

    // http://bootstrap-table.wenzhixin.net.cn/documentation/#localizations
    globals.tableLocale = {
        formatNoMatches: function () {
            return 'В этом городе нет предложений по выбранной валюте';
        }
    };

    return globals;

});

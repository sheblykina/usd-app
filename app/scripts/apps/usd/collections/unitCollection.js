/**
 * Created by andreybobrov on 8/6/15.
 */
define(function(require) {
    'use strict';

    var Backbone = require('backbone');
    var Unit = require('../models/unitModel');
    var globals = require('../../../globalConfig');

    var UnitCollection = Backbone.Collection.extend({
        baseUrl: globals.marketBaseUri,

        url: globals.usdApiUri,

        model: Unit,

        city: 'kiev',

        currency: 'usd',

        tradeType: '2',

        initialize: function (options) {
            this.updateUrl(options || {});
        },

        fetch: function (options) {
            this.trigger('fetch', this, options);
            return Backbone.Collection.prototype.fetch.call(this, options);
        },

        updateUrl: function (args) {
            if (args.city)
                this.city = args.city;
            if (args.currency)
                this.currency = args.currency;
            if (args.tradeType)
                this.tradeType = args.tradeType;

            this.url = this.baseUrl + this.city + "/" + this.currency + "/" + this.tradeType;
        }
    });

    return UnitCollection;
});

/**
 * Created by andreybobrov on 9/22/15.
 */
define(function(require) {
    'use strict';

    var Backbone = require('backbone');
    var UserOffer = require('../models/userOfferModel');
    var globals = require('../../../globalConfig');
    var UnitCollection = require('./unitCollection');

    var UserOffersCollection = UnitCollection.extend({
        url: globals.userOffersUri,

        model: UserOffer,

        initialize:function(){

        }

    });

    return UserOffersCollection;
});

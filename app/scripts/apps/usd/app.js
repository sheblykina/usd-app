/**
 * Created by andreybobrov on 8/6/15.
 */
define(function(require){
    'use strict';

    var MainView = require('./views/mainView');
    var UserOffersView = require('./views/userOffersView');
    var unitCollection = require('./collections/unitCollection');

    var usdApp = {

        run:function(viewManager){

            var mainView = new MainView();
            viewManager.show(mainView);

        },

        runUser:function(viewManager){
            var view = new UserOffersView();
            viewManager.show(view);
        }
    };
    return usdApp;

});

/**
 * Created by andreybobrov on 8/12/15. qwert
 */
define( function(require) {
    'use strict';

    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');
    var datasource = require('./dropDownDataSource');


    // We really need to require this extension in order to make drop-down work
    require('bootstrap/dropdown');

    var CityDropdrownView = Backbone.View.extend({

        tagName: 'div',

        className: 'dropdown width-justified-mobile',

        template: templates.cityDropdownTemplate,

        dataSource: datasource,

        initialize: function (options) {
            this.city = options ? options.city || "kiev" : "kiev";
        },

        render: function () {
            var that = this;
            var contents = _.template(this.template, {cities: this.dataSource});
            this.$el.html(contents);

            //fix for dropDown not expanding
            this.$el.find(".dropdown-toggle").dropdown();

            //initial value
            var $cityButtonText = this.$cityButtonText = this.$el.find("#cityMenu-btn-text");
            $cityButtonText.text(this.dataSource[this.city]);

            this.$el.find('#city-item').click(function (e) {
                e.preventDefault();
                // gets the 'value' attribute, which stores city name
                var city = e.currentTarget.attributes.getNamedItem("value").value;
                if (that.city != city) {
                    that.city = city;

                    var cityName = that.dataSource[city];
                    that.$cityButtonText.text(cityName);

                    that.trigger("change:city", {city: city, cityName: cityName});
                }
            });

        },

        setCityText: function (city) {
            if (this.city != city) {

                this.city = city;

                var cityName = this.dataSource[this.city];
                this.$cityButtonText.text(cityName);

                this.trigger("change:city", {city: city, cityName: cityName});
            }
        }

    });

    return CityDropdrownView;
});

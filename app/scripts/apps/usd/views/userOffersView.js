/**
 * Created by andreybobrov on 9/22/15.
 */
/**
 * Created by andreybobrov on 8/6/15.
 */

// TODO: Refactor as per http://www.toptal.com/backbone-js/top-8-common-backbone-js-developer-mistakes

define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');
    var UserOffersCollection = require('../collections/userOffersCollection');
    var TableView = require('./tableView');
    var CityDropdrownView = require('./cityDropDownView');
    var TradeTypeSelectorView = require('./tradeTypeSelectorView');
    var Cookies = require('js-cookie');
    var UserOfferModel = require('../models/userOfferModel');

    var UserOffersView = Backbone.View.extend({

        $spinner: $('#loading'),

        className: 'container',

        tableTemplate: templates.userOffersTable,

        cookieName: 'user_form_prefill',

        cookieObject: Object.create(null),

        selectedCurrency: "",

        model: {},
        //TODO: http://backbonejs.org/#View-events instead of Jquery

        initialize: function () {
            var that = this;

            this.collection = new UserOffersCollection();

            this.collection.on('reset', this.renderTable, this);

            this.collection.on('update', this.renderTable, this);

            this.collection.on('fetch', function () {
                that.$spinner.fadeIn('fast');
            });

            this.model = new UserOfferModel();

            this.model.set("isNew", true);

            this.model.on('sync', function () {
                that.$spinner.fadeOut('fast');
            });

            this.setInitialPreferencesFromCookies();
        },

        setInitialPreferencesFromCookies: function () {
            this.cookieObject = Cookies.getJSON(this.cookieName) || {};
        },

        renderPlainHtmlContainers: function () {
            var that = this;

            that.$el.append('\
            <div class="row">\
                <div class="form-group col-xs-6 col-sm-4 col-md-3 col-lg-3 pull-left" id="buttons-container">\
                </div>\
            </div>\
            <div class="row">\
                <div class="col-xs-12" id="form-container"></div>\
            </div>\
            <div class="row">\
                <div class="col-xs-12" id="alert-container"></div>\
            </div>\
            <div class="row">\
                <div class="col-xs-12" id="info-container"></div>\
            </div>\
            <div class="row table-margin">\
                <div class="col-xs-12" id="table-container"></div>\
            </div>\
            ');

            that.$tableContainer = $('#table-container', that.$el);
            that.$formContainer = $('#form-container', that.$el);
        },

        renderToggleButton: function () {

            var that = this;
            var $buttonsContainer = $('#buttons-container', that.$el);
            var html = '\
            <div class="width-justified-tablet" id="btnToggleForm">\
                <button class="btn btn-info width-justified" type="submit" id="btnAccept">\
                    <span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span><strong> Добавить</strong>\
                </button>\
            </div>';
            $buttonsContainer.append(html);
            // Form should be hidden at start
            that.$formContainer.hide();
            that.$btnToggleForm = $("#btnToggleForm", $buttonsContainer);
            // EVENTS
            that.$btnToggleForm.click(function (e) {
                that.$formContainer.show(400);
                that.$formContainer.find('#user-phone').focus();
                that.$btnToggleForm.hide();
            });
        },

        renderForm: function () {
            var that = this;

            var temp = templates.createOfferFormTemplate;
            var $html = _.template(temp);
            that.$formContainer.append($html);
            var $form = that.$form = that.$formContainer.find("#createOfferForm");

            //FIXFIX for phone control
            require('bfh/bootstrap-formhelpers-phone');
            var phoneInput = this.phoneInput = this.$form.find('#user-phone');
            phoneInput.bfhphone(phoneInput.data());
            phoneInput.val(this.cookieObject.phone || "");
            phoneInput.on("focus", function () {
                phoneInput.parent().parent().removeClass("has-error");
            });

            require('jquery-plugins-numeric');
            var amountInput = this.$form.find("#amount");
            amountInput.numeric({decimal: false, negative: false});
            var rateInput = this.$form.find("#rate");
            rateInput.numeric({decimal: ",", decimalPlaces: 2, negative: false});

            // INITIALIZE the bootstrap dropdown
            var $dropDownButton = $("#btnDropDown", $form);
            require('bootstrap/dropdown');
            $dropDownButton.dropdown();

            // EVENTS

            // Prevent default submit action
            $form.submit(function (e) {
                e.preventDefault();
            });

            var $currencyDropDown = $form.find('#currencyDropDown');

            $currencyDropDown.find("a").click(function (e) {
                e.preventDefault();

                // Set the sign of the selected value
                var currencySign = e.currentTarget.textContent;
                $dropDownButton.text(currencySign);

                that.selectedCurrency = e.currentTarget.attributes.getNamedItem("value").value;
            });

            // To set the initial value of the dropdown
            $currencyDropDown.find("li>a[value='" + (this.cookieObject.selectedCurrency || "usd") + "']")[0].click();

            $form.find("#btnCancel").click(function (e) {
                //Just hide the form
                that.$formContainer.hide();
                that.$btnToggleForm.show(400);
            });

            $form.find("#btnAccept").click(function (e) {
                // intended
                if (!(that.collection.size() < 3)) {
                    that.displayLimitWarn();
                    return;
                }

                var res = that.$form[0].checkValidity();
                if (!res) {
                    return;
                }

                var phone = that.phoneInput.val();
                // Simple validation
                if (phone.length != 15) {
                    that.phoneInput.parent().parent().addClass("has-error");
                    return;
                }
                var city = that.cities.city;
                var tradeType = that.typeSelector.tradeType;
                var currency = that.selectedCurrency;
                var amount = that.$form.find("#amount").val();
                var rate = that.$form.find("#rate").val();
                var comment = that.$form.find("#comment").val();
                var place = that.$form.find("#place").val();

                var cookieObject = that.cookieObject;
                cookieObject.phone = phone;
                cookieObject.city = city;
                cookieObject.tradeType = tradeType;
                cookieObject.currency = currency;
                Cookies.set(that.cookieName, cookieObject);

                var model = that.model || {};
                // TODO: set the isNew flag to false on edit click
                model.set("isNew", (model.get("isNew") != false));
                model.set("phone", phone);
                model.set("city", city);
                model.set("tradeType", tradeType);
                model.set("currency", currency);
                model.set("count", amount);
                model.set("rate", rate);
                model.set("comment", comment);
                model.set("place", place);
                // TODO: implement model validation as per http://backbonejs.org/#Model-validate
                // first parameter is required to trigger the success and error events

                //that.collection.create(model);
                model.save(null, {
                    success: function () {
                        that.refreshTable();
                        that.displaySuccess();
                    },
                    error: function (model, resp) {
                        if (resp.status && resp.status == 400) {
                            //TODO: Show the error message to indicate that something has went wrong
                            that.displayWarn();
                        }
                    }

                });

            });

        },

        renderCityDropDown: function () {
            var that = this;
            var cities = new CityDropdrownView({city: that.cookieObject.city || 'kiev'});
            cities.render();
            this.cities = cities;

            this.$formContainer.find('#cities-container').append(cities.$el);
        },

        renderTradeTypeSelector: function () {
            var that = this;
            var typeSelector = new TradeTypeSelectorView({tradeType: that.cookieObject.tradeType || "2"});
            typeSelector.render();
            this.typeSelector = typeSelector;

            this.$formContainer.find('#tradeType-container').append(typeSelector.$el);
        },

        renderAlert: function () {
            var that = this;
            var alertHtml = '\
            <div class="alert alert-danger alert-dismissible" id="server-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="error-reload" class="glyphicon glyphicon-repeat" aria-hidden="true"></span>\
            </button>\
                Ошибка при загрузке данных с сервера</div>';
            this.$el.find('#alert-container').append(alertHtml);

            this.$alert = this.$el.find('#server-alert');
            this.$alert.hide();

            this.$alert.find("#error-reload").on('click', function () {
                that.collection.reset();
                that.$alert.hide();
            })
        },
        renderInfoPromts: function () {
            var that = this;
            var successHtml = '\
            <div class="alert alert-success alert-dismissible" id="success-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="alert-dismiss" class="glyphicon glyphicon-ok" aria-hidden="true"></span>\
            </button>\
                Спасибо! Ваша заявка появится в списке через пару минут!</div>';

            var warningHtml = '\
               <div class="alert alert-warning alert-dismissible" id="warn-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="alert-dismiss" class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>\
            </button>\
                Неправильные данные!</div> ';

            var adsLimitHtml = '\
               <div class="alert alert-warning alert-dismissible" id="limit-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="alert-dismiss" class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>\
            </button>\
                Лимит превышен. 3/д</div> ';

            var errorDeleteHtml = '\
               <div class="alert alert-warning alert-dismissible" id="error-delete-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="alert-dismiss" class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span>\
            </button>\
                Возникла ошибка при удалении заявки. Повторите через некоторое время</div> ';

            var successDeleteHtml = '\
            <div class="alert alert-success alert-dismissible" id="success-delete-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="alert-dismiss" class="glyphicon glyphicon-ok" aria-hidden="true"></span>\
            </button>\
                Заявка была удалена. Данные обновятся через пару минут.</div>';

            this.$el.find('#info-container').append(successHtml);
            this.$el.find('#info-container').append(warningHtml);
            this.$el.find('#info-container').append(adsLimitHtml);
            this.$el.find("#info-container").append(errorDeleteHtml);
            this.$el.find("#info-container").append(successDeleteHtml);
            //TODO: create a separate view for promts
            var $success = this.$el.find('#success-alert');
            $success.hide();
            $success.find("#alert-dismiss").on('click', function () {
                $success.fadeOut("fast");
            });

            that.displaySuccess = function () {
                $success.show();
                setTimeout(function () {
                    $success.fadeOut("slow");
                }, 7000)
            };

            var $warn = this.$el.find('#warn-alert');
            $warn.hide();
            $warn.find("#alert-dismiss").on('click', function () {
                $warn.fadeOut("fast");
            });

            that.displayWarn = function () {
                $warn.show();
                setTimeout(function () {
                    $warn.fadeOut("slow");
                }, 10000)
            };

            var $lim = this.$el.find('#limit-alert');
            $lim.hide();
            $lim.find("#alert-dismiss").on('click', function () {
                $lim.fadeOut("fast");
            });

            that.displayLimitWarn = function () {
                $lim.show();
                setTimeout(function () {
                    $lim.fadeOut("slow");
                }, 10000)
            };

            var $delOk = this.$el.find('#success-delete-alert');
            $delOk.hide();
            $delOk.find("#alert-dismiss").on('click', function () {
                $delOk.fadeOut("fast");
            });

            that.displayDeleteOk = function () {
                $delOk.show();
                setTimeout(function () {
                    $delOk.fadeOut("slow");
                }, 10000)
            };

            var $delErr = this.$el.find('#error-delete-alert');
            $delErr.hide();
            $delErr.find("#alert-dismiss").on('click', function () {
                $delErr.fadeOut("fast");
            });

            that.displayDeleteError = function () {
                $delErr.show();
                setTimeout(function () {
                    $delErr.fadeOut("slow");
                }, 10000)
            }
        },

        refreshTable: function () {
            var that = this;

            this.collection.fetch({
                success: function (res) {
                    that.$alert.hide();
                    that.$spinner.fadeOut('fast');

                    that.renderTable(res);
                },

                error: function (collection, res) {
                    that.$spinner.fadeOut('fast');

                    // for security reasons
                    if (res.status == 403) {
                        window.router.navigate("", {trigger: true});
                        return;
                    }
                    that.$alert.show();
                }
            });
        },

        renderTable: function (collection) {
            var that = this;
            var $table = $('#unit-list', this.$tableContainer);

            if ($table) {
                $table.remove();
            }

            var table = new TableView({
                collection: collection || this.collection,
                selectedCurrency: this.selectedCurrency,
                template: this.tableTemplate
            });

            var contents = table.$el;

            table.render();
            this.$tableContainer.append(contents);

            this.$tableContainer.find("#offer-delete").on("click", function (e) {
                var target = e.currentTarget;
                var offerId = target.attributes["data-id"].value;
                var model = that.collection.remove(offerId);
                model.destroy({
                    success: function (modl, resp) {
                        that.displayDeleteOk();
                    },
                    error: function (modl, res, err) {
                        console.error("error while deleting model:" + model._id);
                        console.error(err);
                        that.displayDeleteError();
                    }
                });

            });
        },


        render: function () {
            this.renderPlainHtmlContainers();
            this.renderToggleButton();
            this.renderForm();
            this.renderTradeTypeSelector();
            this.renderCityDropDown();
            this.renderAlert();
            this.renderInfoPromts();
            this.refreshTable();
        }

    });

    return UserOffersView;
});

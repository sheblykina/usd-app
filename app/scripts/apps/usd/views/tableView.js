/**
 * Created by andreybobrov on 8/6/15.
 */
define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');
    var sort = require('bootstrap-table');
    var globals = require('../../../globalConfig');
    var tableLocale = globals.tableLocale;
    var sign = '';

    var currencyFormatter = function(value) {
        if(value) {
            return sign + " " + value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        return "";
    };

    window.currencyValueFormatter = currencyFormatter;

    var TableView = Backbone.View.extend({

        tagName: 'div',
        className: 'table-responsive',
        id: 'unit-list',
        template: templates.marketOffersTable,

        initialize: function (args) {
            sign = globals.signs[args.selectedCurrency];
            this.template = args.template||templates.marketOffersTable;
        },

        render: function () {
            var contents = _.template(this.template,
                { units: this.collection.toJSON() });
            this.$el.html(contents);
            this.$table = this.$el.find('offers-data-table');

            this.$table.bootstrapTable(tableLocale);

        },

        handleCurrencyChanged:function(newCurrencyArgs) {
            sign = globals.signs[newCurrencyArgs.selectedCurrency];
        }
    });

    return TableView;
});

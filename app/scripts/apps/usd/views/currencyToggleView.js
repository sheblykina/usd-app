/**
 * Created by andreybobrov on 8/11/15.
 */
define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');


    var CurrencyToggle = Backbone.View.extend({

        tagName: 'div',

        className: 'btn-group btn-group-justified',

        attributes: {
            "data-toggle": "buttons"
        },

        template: templates.currencyViewTemplate,

        initialize: function (options) {
            this.selectedCurrency = options.selectedCurrency;
        },

        render: function () {
            var that = this;

            var contents = _.template(this.template);
            this.$el.html(contents);

            // set the initial value
            this.$el.find(".active").removeClass('active');
            this.$el.find("#currency[value='" + this.selectedCurrency + "']").parent().addClass('active');

            // handle the click
            this.$el.find('#currency').click(function (e) {
                e.preventDefault();
                var currency = e.currentTarget.value;

                if (that.selectedCurrency != currency) {
                    that.selectedCurrency = currency;
                    $('.active', that.$el).removeClass('active');
                    $(e.currentTarget, that.$el).parent().addClass('active');

                    that.trigger("change:currency", {selectedCurrency: currency});
                }
            });
        },

        setActiveCurrency: function (value) {
            if (this.selectedCurrency != value) {

                this.selectedCurrency = value;

                this.$el.find(".active").removeClass('active');
                this.$el.find("#currency[value='" + value + "']").parent().addClass('active');

                this.trigger("change:currency", {selectedCurrency: value});
            }
        }
    });

    return CurrencyToggle;
});

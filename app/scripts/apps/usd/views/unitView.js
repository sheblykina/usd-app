/**
 * Created by andreybobrov on 8/9/15.
 */

define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');

    var UnitView = Backbone.View.extend({

        template: _.template(templates.marketItemTemplate),

        render: function () {
            var html = this.template(this.model.toJSON());
            //this.setElement( html);
            this.$el = html;
            //$(this.el).html(html);
            return this;
        }

    });

    return UnitView;
});

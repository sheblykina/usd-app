/**
 * Created by andreybobrov on 8/13/15.
 */
define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');

    var TradeTypeSelectorView = Backbone.View.extend({

        tagName:'div',

        className:'btn-group btn-group-justified',

        attributes: {
            "data-toggle":"buttons"
        },

        template: templates.tradeTypeViewTemplate,

        initialize: function (options) {
            this.tradeType = options ? options.tradeType || "2" : "2";
        },

        render: function () {
            var that = this;

            var contents = _.template(that.template);
            that.$el.html(contents);

            that.$el.find('#tradeType').click(function (e) {
                e.preventDefault();
                var tradeType = e.currentTarget.value;

                if (tradeType != that.tradeType) {
                    that.$el.find('.active').removeClass('active');
                    that.$el.find(e.currentTarget).parent().addClass('active');
                    that.tradeType = tradeType;
                    that.trigger("change:tradeType", {tradeType: tradeType});
                }
            });

            // Initial value
            this.$el.find(".active").removeClass('active');
            this.$el.find("#tradeType[value='" + this.tradeType + "']").parent().addClass('active');
        },

        setTypeText:function(tradeType) {
            if (this.tradeType != tradeType) {
                this.tradeType = tradeType;
                this.$el.find(".active").removeClass('active');
                this.$el.find("#tradeType[value='" + tradeType + "']").parent().addClass('active');
            }
        }

    });

    return TradeTypeSelectorView;
});

/**
 * Created by andreybobrov on 8/6/15.
 */

// TODO: Refactor as per http://www.toptal.com/backbone-js/top-8-common-backbone-js-developer-mistakes
// TODO: Refactor: Remove $ -> use find instead
define(function(require) {
    'use strict';
    var Backbone = require('backbone');
    var _ = require('underscore');
    var templates = require('./templates');
    var UnitCollection = require('../collections/unitCollection');
    var TableView = require('./tableView');
    var CurrencyPills = require('./currencyToggleView');
    var CityDropdrownView = require('./cityDropDownView');
    var TradeTypeSelectorView = require('./tradeTypeSelectorView');
    var Cookies = require('js-cookie');

    //TODO: Add a model
    var MainView = Backbone.View.extend({

        $spinner: $('#loading'),

        className: 'container',

        cookieName: 'orders_preferences',

        cookieObject:Object.create(null),
        //TODO: http://backbonejs.org/#View-events instead of Jquery

        initialize: function () {
            var that = this;

            this.collection = new UnitCollection();
            this.selectedCurrency = this.collection.currency;

            this.collection.on("reset", this.renderTable, this);

            this.collection.on('fetch', function () {
                that.$spinner.fadeIn('fast');
            });

            this.fillCollectionParametersFromCookies();
        },

        renderPlainHtmlContainers: function(){

            var that = this;
            that.$el.append('\
                <div class="row" id="options-container"> \
                <div class="col-xs-12 col-sm-2 col-md-1" id="cities-container"></div>\
                <div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-3" id="currency-container"></div>\
                <div class="col-xs-12 col-sm-4 col-md-3 pull-right" id="tradeType-container"></div> \
                </div>\
                <div class="row">\
                <div class="col-xs-12" id="alert-container"></div>\
                </div>\
                <div class="row table-margin">\
                <div class="col-xs-12" id="table-container"></div>\
                </div> ');
            that.$tableContainer = $('#table-container', that.$el);

            // Below is the in-table spinner

            //that.$tableContainer.append('<div id="loading">\
            //<div id="spinner" class="dots-loader"></div>\
            //</div>');
            //that.spinner = that.$tableContainer.find('#loading');
            //that.spinner.hide();
            //that.collection.on('fetch', function () {
            //    that.spinner.fadeIn('fast');
            //});

            that.$options = $('#options-container', that.$el);
        },

        renderCurrencyPills: function() {
            var that = this;
            var pills = new CurrencyPills({selectedCurrency:that.collection.currency});

            pills.render();
            pills.on("change:currency", function(args) {
                that.handleCurrencyChanged({selectedCurrency: args.selectedCurrency, resetCollection: true});
            });

            that.$options.find('#currency-container').append(pills.$el);
            that.currencyPills = pills;

            that.handleCurrencyChanged = function (args) {
                var currency = args.selectedCurrency;
                if (!currency) {
                    return;
                }

                that.selectedCurrency = currency;

                // Change the appropriate cookie value
                that.cookieObject.selectedCurrency = currency;
                Cookies.set(that.cookieName, that.cookieObject);

                // Update the collection url
                that.collection.updateUrl({currency: currency});

                if (args.resetCollection) {
                    that.collection.reset();
                }
            };
        },

        renderCityDropDown:function(){
            var that = this;
            var cities = new CityDropdrownView({city:that.collection.city});

            cities.render();
            cities.on("change:city", function(args) {
                that.handleCityChanged({city: args.city, cityName: args.cityName, resetCollection: true});
            });

            that.$options.find('#cities-container').append(cities.$el);
            that.cities = cities;

            that.handleCityChanged = function(args) {
                var city = args.city;
                var cityName = args.cityName;

                if (!(cityName&&city)) {
                    return;
                }

                // Change the appropriate cookie value
                that.cookieObject.city = city;
                that.cookieObject.cityName = cityName;
                Cookies.set(that.cookieName, that.cookieObject);

                that.collection.updateUrl({city: city});

                if (args.resetCollection) {
                    that.collection.reset();
                }
            };
        },

        renderTradeTypeSelector:function(){
            var that = this;
            var typeSelector = new TradeTypeSelectorView({tradeType:this.collection.tradeType});

            typeSelector.render();
            typeSelector.on("change:tradeType",function(args) {
                that.handleTradeTypeChanged({tradeType: args.tradeType, resetCollection: true});
            });

            this.$options.find('#tradeType-container').append(typeSelector.$el);
            this.typeSelector = typeSelector;

            this.handleTradeTypeChanged = function(args) {
                var tradeType = args.tradeType;

                if (!tradeType)
                    return;

                // Change the appropriate cookie value
                that.cookieObject.tradeType = tradeType;
                Cookies.set(that.cookieName, that.cookieObject);

                that.collection.updateUrl({tradeType: tradeType});

                if (args.resetCollection) {
                    that.collection.reset();
                }
            };

        },

        renderAlert: function(){
            var that = this;
            var alertHtml = '\
            <div class="alert alert-danger alert-dismissible" id="server-alert" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
            <span id="error-reload" class="glyphicon glyphicon-repeat" aria-hidden="true"></span>\
            </button>\
                Ошибка при загрузке данных с сервера</div>';
            this.$el.find('#alert-container').append(alertHtml);

            this.$alert = this.$el.find('#server-alert');
            this.$alert.hide();

            this.$alert.find("#error-reload").on('click', function(){
                that.collection.reset();
                that.$alert.hide();
            })
        },

        renderTable: function() {
            var $table = this.$tableContainer.find('#unit-list');

            if($table) {
                $table.remove();
            }

            var that = this;

            this.collection.fetch({
                success:function(res){
                    that.$alert.hide();
                    that.$spinner.fadeOut('fast');

                    //TODO: think how to imporve this shit
                    var table = new TableView({collection:res, selectedCurrency:that.selectedCurrency});
                    var contents = table.$el;

                    table.render();
                    that.$tableContainer.append(contents);

                },

                error:function() {
                    that.$spinner.fadeOut('fast');
                    that.$alert.show();
                }
            });
        },

        //TODO: Improve logic of cookies storage
        // Maybe this logic can be improved in future
        fillCollectionParametersFromCookies:function() {
            var json = Cookies.getJSON(this.cookieName);
            if (!json || json == undefined) {
                return;
            }
            this.cookieObject = json;
            var currency = !json.selectedCurrency ? this.collection.currency : json.selectedCurrency;
            var city = !json.city ? this.collection.city : json.city;
            var tradeType = !json.tradeType ? this.collection.tradeType : json.tradeType;

            this.collection.updateUrl({city: city, currency: currency, tradeType: tradeType});
        },

        render: function () {
            this.renderPlainHtmlContainers();
            this.renderTradeTypeSelector();
            this.renderCurrencyPills();
            this.renderCityDropDown();
            this.renderAlert();
            this.renderTable();
        }

    });

    return MainView;
});

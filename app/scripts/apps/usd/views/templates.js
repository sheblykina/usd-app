/**
 * Created by andreybobrov on 8/6/15.
 */
define(function(require) {
    'use strict';

    var templates = {

        marketOffersTable: '\
        <table class="table table-hover table-no-bordered" data-toggle="table" data-sort-name="time" data-sort-order="desc" id="offers-data-table"> \
            <thead class="color-light-grey">\
                <tr>\
                <th class="col-xs-2 border-straight">Телефон</th>\
                <th class="col-xs-1" data-sortable="true" data-field="count" data-formatter="currencyValueFormatter">Кол-во</th>\
                <th class="col-xs-1" data-sortable="true" data-field="rate">Курс</th>\
                <th class="col-xs-1" data-sortable="true" data-field="time">Время</th>\
                <th class="hidden-xs">Место</th>\
                <th class="hidden-xs">Комментарий</th>\
                </tr>\
            </thead>\
            <tbody>\
        <% var adIndex = 10; %>\
        <% _.forEach(units, function (unit) { %> \
                <%if(adIndex>0){%>\
                <tr> \
                    <td><%- unit.phone %></td>\
                    <td><%- unit.count %></td>\
                    <td><%- unit.rate %></td>\
                    <td><%- unit.time %></td>\
                    <td class="hidden-xs"><%- unit.place %></td>\
                    <td class="hidden-xs"><%- unit.comment %></td>\
                </tr> \
                <% adIndex--;%>\
                <% }%>\
                <%if(adIndex==0){ %>\
                <!--put an ad placeholder here-->\
                <% adIndex = 10;%>\
                <% }%>\
        <% }); %>\
         </tbody>\
         </table>',//\
        // </div>',

        userOffersTable: '\
        <table class="table table-hover table-no-bordered" data-toggle="table" data-sort-name="time" data-sort-order="desc"> \
            <thead class="color-light-grey">\
                <tr>\
                <th class="col-xs-2 col-sm-3 col-md-2 border-straight">Действия</th>\
                <th class="col-xs-1 col-sm-2 col-md-1" data-sortable="true" data-field="count" data-formatter="currencyValueFormatter">Кол-во</th>\
                <th class="col-xs-1" data-sortable="true" data-field="rate">Курс</th>\
                <th class="col-xs-1" data-sortable="true" data-field="time">Время</th>\
                <th class="hidden-xs">Место</th>\
                <th class="hidden-xs">Комментарий</th>\
                </tr>\
            </thead>\
            <tbody>\
            <% _.forEach(units, function (unit) { %> \
                <tr class="<%= unit.pendingDelete ? \'warning\':\'\' %>" data-id="<%= unit._id %>">\
                    <td>\
                        <button type="button" class="btn btn-default btn-sm" id="offer-edit" data-id="<%= unit._id %>" disabled="true">\
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>\
                        </button>\
                        <button type="button" class="btn btn-default btn-sm" id="offer-resubmit" data-id="<%= unit._id %>" disabled="<%= !unit.pendingDelete %>" >\
                            <span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>\
                        </button>\
                        <button type="button" class="btn btn-default btn-sm" id="offer-delete" data-id="<%= unit._id %>">\
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>\
                        </button>\
                    </td>\
                    <td><%- unit.count %></td>\
                    <td><%- unit.rate %></td>\
                    <td><%- unit.time %></td>\
                    <td class="hidden-xs"><%- unit.place %></td>\
                    <td class="hidden-xs"><%- unit.comment %></td>\
                </tr>\
            <% }); %>\
            </tbody>\
         </table>',

        currencyViewTemplate: '\
            <label class="btn btn-default active border-straight">\
                <input type="radio" name="currency" id="currency" value="usd"/> <strong>USD</strong> \
                <span class="glyphicon glyphicon-usd glyph-near-text" /> \
            </label>\
            <label class="btn btn-default border-straight">\
                <input type="radio" name="currency" id="currency" value="eur"/> <strong>EUR</strong> \
                <span class="glyphicon glyphicon-eur glyph-near-text" /> \
            </label>\
            <label class="btn btn-default border-straight">\
                <input type="radio" name="currency" id="currency" value="rub"/> <strong>RUB</strong> \
                <span class="glyphicon glyphicon-rub glyph-near-text" /> \
            </label>\
        ',

        tradeTypeViewTemplate: '\
            <label class="btn btn-default border-straight">\
                <input type="radio" name="tradeType" id="tradeType" value="1"/> <strong>Покупка</strong> \
            </label>\
            <label class="btn btn-default active border-straight">\
                <input type="radio" name="tradeType" id="tradeType" value="2"/> <strong>Продажа</strong> \
            </label>\
            \
        ',

        cityDropdownTemplate:'\
        <button class="btn btn-default dropdown-toggle border-straight width-justified-mobile width-justified-tablet" type="button" id="cityMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">\
            <span id="cityMenu-btn-text">Dropdown</span>\
            <span class="caret"></span>\
        </button>\
        <ul class="dropdown-menu scrollable-menu width-justified-mobile" aria-labelledby="cityMenu">\
        <% _.forEach(cities.keys, function(city){ %>\
            <li><a href="#" id="city-item" value="<%-city %>" ><%-cities[city] %></a></li>\
            <% }); %>\
        </ul>',

        createOfferFormTemplate:'\
        <form class="panel panel-default panel-body color-light-grey" id="createOfferForm">\
            <!-- phone and count-->\
                <div class="form-group col-xs-12 col-sm-6 col-md-3 has-error">\
                    <div class="input-group">\
                    <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>\
                        <input type="text" id="user-phone" class="form-control input-medium bfh-phone" data-format="+38 ddd ddddddd" placeholder="+38 099 9998877" required>\
                    </div>\
                </div>\
                <div class="form-group col-xs-12 col-sm-6 col-md-3">\
                    <div class="input-group col-xs-12">\
                        <div class="input-group-btn">\
                        <button type="button" class="btn btn-default dropdown-toggle form-control" id="btnDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Text<span class="caret"></span></button>\
                            <ul class="dropdown-menu" id="currencyDropDown" aria-labelledby="btnDropDown">\
                                <li><a href="#" id="dropdownitem" value="usd">$</a></li>\
                                <li><a href="#" id="dropdownitem" value="eur">&#8364;</a></li>\
                                <li><a href="#" id="dropdownitem" value="rub">&#x20bd;</a></li>\
                            </ul>\
                        </div><!-- /btn-group -->\
                        <div class="col-xs-6 no-side-padding"> \
                            <input type="text" id="amount" class="form-control" placeholder="Кол-во" required minlength="1" maxlength="8" \
                            oninvalid="this.setCustomValidity( \'Обязательное поле\' )" oninput="setCustomValidity(\'\')" />\
                        </div>\
                        <div class="col-xs-6 no-side-padding"> \
                            <input type="text" id="rate" class="form-control" placeholder="Курс" required minlength="1" maxlength="6" \
                            oninvalid="this.setCustomValidity(\'Обязательное поле\' )" oninput="setCustomValidity(\'\')" />\
                        </div>\
                    </div><!-- /input-group -->\
                </div>\
             <!-- Cities -->\
            <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 no-side-padding-tablet-mobile">\
                <div class="form-group col-xs-12" id="cities-container"></div>\
            </div>\
            <!-- Trade type -->\
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 pull-right no-side-padding-tablet-mobile">\
                <div class="form-group col-xs-12" id="tradeType-container"></div>\
            </div>\
            <!-- Place -->\
            <div class="col-xs-12 col-sm-12 col-md-3">\
                <div class="form-group">\
                    <input type="text" class="form-control" id="place" placeholder="Место">\
                </div>\
            </div>\
            <!-- Comment -->\
            <div class="col-xs-12 col-sm-12 col-md-4 hidden-xs">\
                <div class="form-group">\
                    <input type="text" class="form-control" id="comment" placeholder="Комментарий">\
                </div>\
            </div>\
            <!-- Buttons -->\
            <div class="col-xs-12 col-sm-12 col-md-5 no-side-padding-tablet-mobile" id="formActions">\
                <div class="form-group">\
                    <div id="formActions">\
                        <div class="col-xs-6">\
                            <button class="btn btn-success width-justified" type="submit" id="btnAccept">\
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span><strong> Сохранить</strong>\
                            </button>\
                        </div>\
                        <div class="col-xs-6">\
                            <button class="btn btn-danger width-justified" type="submit" id="btnCancel">\
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span><strong> Отменить</strong>\
                            </button>\
                         </div>\
                    </div>\
                </div>\
            </div>\
        </form>',

        marketItemTemplate: '\
        <tr> \
                <td><%= phone %></td>\
                <td><%= count %></td>\
                <td><%= rate %></td>\
                <td><%= time %></td>\
                <td><%= comment %></td>\
                <td><%= place %></td>\
         </tr>\
        '
    };

    return templates;
});


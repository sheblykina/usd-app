/**
 * Created by andreybobrov on 10/10/15.
 */
define(function(require) {
    'use strict';

    var Backbone = require('backbone');

    var globals = require('../../../globalConfig');

    var UserOfferModel = Backbone.Model.extend({

        //url:globals.userOffersUri,
        idAttribute: "_id",

        url: function () {
            return globals.userOffersUri + "/" + encodeURIComponent(this.attributes[this.idAttribute]);
        }
    });

    return UserOfferModel;
});

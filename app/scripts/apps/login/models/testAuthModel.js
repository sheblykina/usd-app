/**
 * Created by andreybobrov on 9/3/15.
 */
define(function (require) {
    'use strict';

    var Backbone = require('backbone');

    var TestAuthModel = Backbone.Model.extend({
        url: 'http://127.0.0.1:3000/user/current'
    });

    return TestAuthModel;
});

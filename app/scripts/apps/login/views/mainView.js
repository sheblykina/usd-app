define(function (require) {
    'use strict';

    var Backbone = require('backbone');
    var Cookies = require('js-cookie');

    var MainView = Backbone.View.extend({
        template: '\
            <form role="form" class="form form-group">\
                <div class="errors hide">\
                    <p>Sorry, could not authorize you</p>\
                </div>\
                <button type="submit" class="btn btn-lg btn-social btn-twitter" id="login" value="twitter">\
                    <span class="fa fa-twitter"></span>\
                    Login TW\
                </button>\
                <button type="submit" class="btn btn-lg btn-social btn-facebook" id="login" value="fb">\
                    <span class="fa fa-facebook"></span>\
                    Login FB\
                </button>\
                <button type="submit" class="btn btn-lg btn-social btn-vk" id="login" value="vk">\
                    <span class="fa fa-vk"></span>\
                    Login VK\
                </button>\
                <button id="redirectToMe"> RedirectToMe</button>\
            </form>\
        ',

        events: {
            'click #login': 'login',
            'click #testAuth': 'testAuth',
            'click #redirectToMe': 'redirectToMe'
        },

        redirectToMe: function () {
            window.router.navigate('/my/usd', {trigger: true});
        },
        render: function () {
            this.$el.html(this.template);
        },

        login: function (e) {
            e.preventDefault();
            var target = e.currentTarget.attributes.getNamedItem("value").value;
            window.open('http://127.0.0.1:3000/auth/' + target);
            this.checkLoginCookie();
        },

        checkLoginCookie: function () {
            var that = this;
            var intHandle = window.setInterval(function () {
                var val = Cookies.get("auth_completed");
                // only when the cookie is available
                if (val) {
                    window.clearInterval(intHandle);

                    // removing the cookie
                    Cookies.set("auth_completed", "");
                    if (val == "success") {
                        that.redirectToMe();
                    }
                    if (val == "error") {
                        //that.showError();
                    }
                }
            }, 100)
        },

        redirectToList: function () {
            window.router.navigate('/usd', {trigger: true});
        },

        showError: function () {
            this.$('.errors').removeClass('hide');
        }
    });

    return MainView;
});

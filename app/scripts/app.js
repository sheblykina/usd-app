define(function (require) {
    'use strict';
    var Backbone = require('backbone');
    var Cookies = require('js-cookie');

    var ViewManager = require('viewManager');

    var loginApp = require('apps/login/app');
    var usdApp = require('apps/usd/app');

    var authorizationManager = require('./framework/authorizationManager');
    var viewManager = new ViewManager();

    var Router = Backbone.Router.extend({
        routes: {
            '': 'usd',
            'login/auth_close': 'authClose',
            'login': 'login',
            'my': 'myOffers'
        },
        usd: function () {
            usdApp.run(viewManager);
        },
        login: function () {
            loginApp.run(viewManager);
        },
        authClose: function () {
            // TODO: add errors
            Cookies.set("auth_completed", "success");
            window.close();
        },
        myOffers: function () {
            var that = this;
            authorizationManager.checkUserAuth().then(function () {
                usdApp.runUser(viewManager);
            }, function () {
                that.navigate("login", {trigger: true});
            })
        }

    });

    return {
        run: function () {

            // Hiding the spinner once we run
            $("#loading").hide();

            // this allows us to transfer Cookies via XHR
            $.ajaxPrefilter( function( options, originalOptions, jqXHR ) {
                options.xhrFields = {
                    withCredentials: true
                };
                // If we have a csrf token send it through with the next request
                // if(typeof that.get('_csrf') !== 'undefined') {
                //     jqXHR.setRequestHeader('X-CSRF-Token', that.get('_csrf'));
                // }
            });

            // FIXFIX
            // data-toggle doesn't work in our configuration
            // make navbar collapsible

            var navBarCollapse = require('bootstrap/collapse');
            var $nav = $("#nav-main");
            $nav.collapse();

            // FIXFIX
            // navbar should reflect its actual state (active section)
            $nav.find("a").on("click", function(){
                $nav.find(".active").removeClass("active");
                $(this).parent().addClass("active");

                // FIXFIX
                // This collapses the collapsible navbar on a link click
                // https://github.com/twbs/bootstrap/issues/12852

                // check if window is small enough so dropdown is created
                var toggle = $nav.find(".navbar-toggle").is(":visible");
                if (toggle) {
                    $nav.find(".navbar-collapse").collapse('hide');
                }
            });

            // Standard backbone router config
            var router = window.router = new Router();
            Backbone.history.start();
            return router;
        }
    };
});
